# PR Reviewer
This project is created to help the Pull Request process within Your Organisation.

Once configured it will read all created and open Pull Requests. Order them and if needed filter.

It also provides you with statistics.

Project was created in `Python 3.7`

## Project Setup

In order to run the project first you need to install the requirements. You can do so by running following command:

```bash
pip install requirements.txt
```

Once the requirements are met you need to setup the environment variables:

```bash
export SECRET_KEY=your_secret_key
export DATABASE_URL=sqlite:///db.sqlite3
export DEBUG=True
export GITHUB_ACCESS_TOKEN=your_github_access_token
export GITHUB_ORGANIZATION=your_organization
export BROKER_URL=amqp://guest:guest@localhost:5672
```

Run migrations:
```bash
python manage.py migrate
```

Run development server:

```bash
python manage.py runserver
```

## Celery and RabbitMQ
The project is using Celery and RabbitMQ to run some tasks

You need to run rabbitmq, we recommend using docker, for example:
```bash
docker run -d -p 5672:5672 -p 15672:15672 --name rabbitmq rabbitmq:management
```

Next you would need to run celery worker:

```bash
celery -A pr_reviewer worker -l info
```

Running manually tasks can be done with:
```bash
celery call pr_github.tasks.populate_projects
```
