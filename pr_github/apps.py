from django.apps import AppConfig


class PrGithubConfig(AppConfig):
    name = 'pr_github'
