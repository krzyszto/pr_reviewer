from itertools import chain

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q, Count
from django.views.generic import ListView

from pr_github.models import Language, PullRequest, Review
from pr_reviewer.settings import REQUIRED_APPROVES_NUMBER


User = get_user_model()


class LanguageListView(LoginRequiredMixin, ListView):
    model = Language
    template_name = 'languages_list.html'


class PullRequestListView(LoginRequiredMixin, ListView):
    queryset = PullRequest.objects.filter(is_active=True, project__is_active=True, status=PullRequest.OPEN)
    template_name = 'pull_requests.html'

    def get_queryset(self):
        """
        This Method will look for all PRs for a specific language.
        Then it would add number of approved/change request reviews to it.
        After that it would filter out those that already has approves greater than REQUIRED_APPROVES_NUMBER.

        Three sub query sets would then be created to allow some PRs to be on the top,
        some will go straight to the bottom.
        Adding [HOTFIX] in your PR front name would take it to the top of the list.
        [WIP] PRs will go to the bottom.
        """
        basic_queryset = super(
            PullRequestListView, self
        ).get_queryset().filter(
            project__language__name=self.kwargs.get('language')
        ).annotate(
            approves_count=Count(
                Q(reviews__state=Review.APPROVED) | Q(reviews__state=Review.CHANGES_REQUESTED)
            ),
        ).filter(approves_count__lt=REQUIRED_APPROVES_NUMBER).order_by('updated_at')

        high_priority = basic_queryset.filter(name__startswith='[HOTFIX]')

        normal_priority = basic_queryset.exclude(
            name__startswith='[WiP]'
        ).exclude(name__startswith='[HOTFIX]')

        low_priority = basic_queryset.filter(name__startswith='[WiP]')

        return list(chain(high_priority, normal_priority, low_priority))

    def get_context_data(self, *args, object_list=None, **kwargs):
        context = super(PullRequestListView, self).get_context_data()
        context['language'] = self.kwargs.get('language')
        return context


class StatisticsListView(LoginRequiredMixin, ListView):
    queryset = User.objects.prefetch_related('reviews')

    template_name = 'global_statistics.html'

    def get_queryset(self):
        queryset = super(StatisticsListView, self).get_queryset()
        return queryset.annotate(
            approved_prs=Count('reviews', filter=Q(reviews__state=Review.APPROVED)),
            rejected_prs=Count('reviews', filter=Q(reviews__state=Review.REJECTED)),
            commented_prs=Count('reviews', filter=Q(reviews__state=Review.COMMENTED)),
            change_requested_prs=Count('reviews', filter=Q(reviews__state=Review.CHANGES_REQUESTED)),
            reviews_count=Count('reviews')
        ).order_by('-reviews_count')
