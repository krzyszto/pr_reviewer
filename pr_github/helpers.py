import requests
from django.contrib.auth import get_user_model

from pr_github.models import PullRequest, Review
from pr_reviewer import status
from pr_reviewer.settings import GITHUB_ACCESS_TOKEN, GITHUB_ORGANIZATION

User = get_user_model()


def user_is_member_of_the_organization(username, organization=GITHUB_ORGANIZATION):
    response = requests.get(
        url=f'https://api.github.com/orgs/{organization}/members/{username}',
        headers=dict(
            Authorization=f'Bearer {GITHUB_ACCESS_TOKEN}'
        )
    )

    if response.status_code == status.HTTP_204_NO_CONTENT:
        return True
    return False


def get_all_repositories_for_language(language, organization=GITHUB_ORGANIZATION):
    response = requests.get(
        url=f'https://api.github.com/search/repositories?q=org:{organization}+language:{language}',
        headers=dict(
            Authorization=f'Bearer {GITHUB_ACCESS_TOKEN}'
        )
    )

    if response.status_code == status.HTTP_200_OK:
        return response.json().get('items')


def get_all_open_prs_for_project(project_url):
    response = requests.get(
        url=f'{project_url}/pulls?state=open',
        headers=dict(
            Authorization=f'Bearer {GITHUB_ACCESS_TOKEN}'
        )
    )

    if response.status_code == status.HTTP_200_OK:
        return response.json()


def get_all_pr_reviews(pr_url):
    response = requests.get(
        url=f'{pr_url}/reviews',
        headers=dict(
            Authorization=f'Bearer {GITHUB_ACCESS_TOKEN}'
        )
    )

    if response.status_code == status.HTTP_200_OK:
        return response.json()


def create_or_update_pull_request(url, project, updated_at, name, username, status):
    pull_request_object = PullRequest.objects.filter(url=url, project=project).first()
    user = User.objects.get_or_create(username=username)[0]
    if not pull_request_object:
        pull_request_object = PullRequest.objects.create(
            url=url,
            project=project,
            updated_at=updated_at,
            name=name,
            user=user,
        )
    if pull_request_object.updated_at != updated_at:
        pull_request_object.updated_at = updated_at
        pull_request_object.name = name
        pull_request_object.user = user
        pull_request_object.status = status
        pull_request_object.save()

    return pull_request_object


def create_or_update_review(username, state_value, pull_request):
    user = User.objects.get_or_create(username=username)[0]

    review = Review.objects.filter(user=user, pull_request=pull_request).first()
    state = Review.get_state(state_value)

    if not review:
        review = Review.objects.create(
            user=user,
            state=state,
            pull_request=pull_request,
        )
    if review.state != state:
        review.state = state
        review.save()
    return review
