from django.core.exceptions import PermissionDenied

from pr_github.helpers import user_is_member_of_the_organization


def check_user(backend, details, response, *args, **kwargs):
    if backend.name == 'github':
        if not user_is_member_of_the_organization(details.get('username')):
            raise PermissionDenied()
