from __future__ import absolute_import, unicode_literals

from celery import shared_task

from pr_github.helpers import get_all_repositories_for_language, get_all_open_prs_for_project, get_all_pr_reviews, \
    create_or_update_pull_request, create_or_update_review
from pr_github.models import Language, Project, PullRequest


@shared_task
def populate_projects():
    for language in Language.objects.all():
        repositories = get_all_repositories_for_language(language)
        for repository in repositories:
            name = repository['name']
            url = repository['url']

            Project.objects.get_or_create(
                name=name,
                url=url,
                language=language
            )


@shared_task
def get_open_prs():
    PullRequest.objects.update(status=PullRequest.CLOSED)
    for project in Project.objects.filter(is_active=True):
        pull_requests = get_all_open_prs_for_project(project_url=project.url)
        for pull_request in pull_requests:
            pull_request_object = create_or_update_pull_request(
                status=PullRequest.OPEN,
                url=pull_request['html_url'],
                updated_at=pull_request['updated_at'],
                name=pull_request['title'],
                username=pull_request['user']['login'],
                project=project,
            )

            for review in get_all_pr_reviews(pr_url=pull_request['url']):
                create_or_update_review(
                    pull_request=pull_request_object,
                    state_value=review['state'],
                    username=review['user']['login'],
                )
