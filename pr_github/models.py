import datetime

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.functional import cached_property
from django.utils.timezone import utc

User = get_user_model()


class Language(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=256, unique=True)
    url = models.URLField()
    is_active = models.BooleanField(default=True)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class PullRequest(models.Model):
    OPEN = 0
    CLOSED = 1
    STATUSES = (
        (OPEN, 'open'),
        (CLOSED, 'closed'),
    )

    name = models.CharField(max_length=256)
    url = models.URLField(unique=True)
    is_active = models.BooleanField(default=True)
    project = models.ForeignKey(Project, related_name='pull_requests', on_delete=models.CASCADE)
    status = models.SmallIntegerField(choices=STATUSES, default=OPEN)
    updated_at = models.DateTimeField()
    user = models.ForeignKey(User, null=True, related_name='pull_requests', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    @cached_property
    def waiting_time(self):
        return datetime.datetime.utcnow().replace(tzinfo=utc) - self.updated_at


class Review(models.Model):
    class Meta:
        unique_together = (('user', 'pull_request'),)

    APPROVED = 0
    REJECTED = 1
    COMMENTED = 2
    CHANGES_REQUESTED = 3

    STATUSES = (
        (APPROVED, 'approved'),
        (REJECTED, 'rejected'),
        (COMMENTED, 'commented'),
        (CHANGES_REQUESTED, 'changes required'),
    )

    user = models.ForeignKey(User, null=True, related_name='reviews', on_delete=models.CASCADE)
    pull_request = models.ForeignKey(PullRequest, null=True, related_name='reviews', on_delete=models.CASCADE)
    state = models.SmallIntegerField(choices=STATUSES)

    def __str__(self):
        return f'{self.user} {self.pull_request}'

    @classmethod
    def get_state(cls, state_value):
        if state_value == 'APPROVED':
            return cls.APPROVED
        elif state_value == 'REJECTED':
            return cls.REJECTED
        elif state_value == 'COMMENTED':
            return cls.COMMENTED
        elif state_value == 'CHANGES_REQUESTED':
            return cls.CHANGES_REQUESTED
        else:
            raise Exception(f'Not expected state_value: {state_value}')
