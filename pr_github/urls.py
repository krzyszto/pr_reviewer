from django.urls import path

from pr_github import views


urlpatterns = [
    path('languages/', views.LanguageListView.as_view(), name='language_list'),
    path('languages/<language>/pull_requests', views.PullRequestListView.as_view(), name='pull_request_list'),

    path('statistics/', views.StatisticsListView.as_view(), name='statistics_list'),
]
